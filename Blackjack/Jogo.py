from Baralho import Baralho
from Jogador import Jogador
class Jogo:
    def start(self, jogador1, jogador2, deck):
        jogador1.inicializaDeck(deck)
        jogador2.inicializaDeck(deck)
    def deckJogador(self, player):
        print("----------------------------------")
        for i in range(len(player.getDeck())):
             print(player.getDeck()[i].nome)
        print("----------------------------------")
    def blackjack(self, pontuacao):
         if pontuacao < 21:
             return True
         if pontuacao == 21:
             print("Vinte E UM!!")
             return False
         else:
             return False
    def jogada(self,jogador, deck):
        if self.blackjack(jogador.somatorio()):
            jogador.retiraCartaDeck(deck)
            if not self.blackjack(jogador.somatorio()):
                return False
        return True
    def probabilidadeJogada(self, jogador, deck):
        possiveisRes = 0
        for i in range(len(deck)):
            if (deck[i].valor + jogador.somatorio()) <= 21:
                possiveisRes += 1
        probabilidade = possiveisRes / len(deck)
        return probabilidade * 100
    def verificaVencedor(self, jogador1,  jogador2):
        if jogador1.somatorio() > jogador2.somatorio() and jogador2.somatorio() < 21 and jogador1.somatorio() < 21:
            print("Você Venceu")
        if jogador1.somatorio() == jogador2.somatorio() and (jogador1.somatorio() < 21 and jogador2.somatorio() < 21):
            print("Empate")
        if jogador1.somatorio() < jogador2.somatorio() and jogador1.somatorio() < 21 and jogador2.somatorio() < 21:
            print("Openente Venceu")
        if jogador1.somatorio() > 21:
            print("Openente Venceu!! voce execedeu pontos acima de 21")
        if jogador2.somatorio() > 21:
            print("Você Venceu!! oponente execedeu pontos acima de 21")

        print("Seu deck: ")
        self.deckJogador(jogador1)
        print("Pontuação: {}".format(jogador1.somatorio()))
        print("Deck oponente: ")
        self.deckJogador(jogador2)
        print("Pontuação: {}".format(jogador2.somatorio()))



