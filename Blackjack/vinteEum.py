from builtins import print
from Jogador import *
from Jogo import Jogo

baralho = Baralho()
baralho.setBaralho()
baralhoDeck = baralho.getDeck()
jogador = Jogador()
jogador1 = Jogador()

jogo = Jogo()
jogo.start(jogador, jogador1, baralhoDeck)
opcao = ""
print("Bem Vindo Ao Jogo")
while opcao != "SAIR":
    print("--------------------------------------")
    print("VER   | ver cartas em mãos")
    print("PROB  | ver probabilidade")
    print("QTD   | quantidade de cartas na pilha")
    print("NOVA  | pegar nova carta")
    print("PARAR | não pegar mais cartas")
    print("SAIR  | sair do jogo")
    print("--------------------------------------")
    opcao = input().upper()
    if opcao == "VER":
        jogo.deckJogador(jogador)
    elif opcao == "PROB":
        print("Probabilidade Sua = {:.2f}%".format(jogo.probabilidadeJogada(jogador, baralhoDeck)))
        print("Probabilidade Oponente = {:.2f}%".format(jogo.probabilidadeJogada(jogador1, baralhoDeck)))

    elif opcao == "QTD":
        print("Numero de Cartas na Pilha = {}".format(len(baralhoDeck)))
    elif opcao == "NOVA":
        if jogo.jogada(jogador, baralhoDeck):
            jogo.verificaVencedor(jogador, jogador1)
            if jogo.jogada(jogador1, baralhoDeck):
                jogo.verificaVencedor(jogador, jogador1)
        else:
            jogo.verificaVencedor(jogador, jogador1)
    elif opcao == "PARAR":
        print("Seu Deck: ")
        jogo.deckJogador(jogador)
        print("Deck oponente: ")
        jogo.deckJogador(jogador1)
    else:
        print("FIM!")
        exit()

