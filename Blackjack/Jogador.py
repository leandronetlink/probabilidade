from Carta import *
from Baralho import Baralho
import random
class Jogador:
    deck = list()
    def inicializaDeck(self, deck):
        deckJogador = list()
        rand = random.randint(0, (len(deck) - 1))
        deckJogador.append(deck.pop(rand))
        rand = random.randint(0, (len(deck) - 1))
        deckJogador.append(deck.pop(rand))
        self.deck = deckJogador
    def getDeck(self):
        return self.deck
    def retiraCartaDeck(self, deck):
        deckJogador = list()
        rand = random.randint(0, (len(deck) - 1))
        deckJogador.append(deck.pop(rand))
        self.deck += deckJogador
    def somatorio(self):
        somaTotal = 0
        for i in range(len(self.deck)):
            somaTotal += self.deck[i].valor
        return somaTotal

